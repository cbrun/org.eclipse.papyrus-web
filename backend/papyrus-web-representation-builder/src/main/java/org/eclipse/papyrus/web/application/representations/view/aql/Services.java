/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.web.application.representations.view.aql;

/**
 * List of service names.
 *
 * @author Arthur Daussy
 */
public class Services {

    public static final String RENDER_LABEL_ONE_LINE = "renderSimpleOneLineLabel"; //$NON-NLS-1$

    public static final String ADD_VALUE_TO_SERVICE = "addValueTo"; //$NON-NLS-1$

    public static final String CREATE_DOMAIN_BASED_EDGE_SERVICE = "createDomainBasedEdge"; //$NON-NLS-1$

    public static final String RECONNECT_SOURCE_ON_DOMAIN_BASED_EDGE = "reconnectSourceOnDomainBasedEdge"; //$NON-NLS-1$

    public static final String RECONNECT_TARGET_ON_DOMAIN_BASED_EDGE = "reconnectTargetOnDomainBasedEdge"; //$NON-NLS-1$

    public static final String CONSUME_DIRECT_EDIT_VALUE_SERVICE = "consumeDirectEditValue"; //$NON-NLS-1$

    public static final String GET_DIRECT_EDIT_INPUT_VALUE_SERVICE = "getDirectEditInputValue"; //$NON-NLS-1$

    public static final String RESET_DEFAULT_NAME_SERVICE = "resetDefaultName"; //$NON-NLS-1$

    public static final String CREATE_SIBLING_SERVICE = "createSibling"; //$NON-NLS-1$

    public static final String DOMAIN_BASED_EDGE_SOURCE_LABEL_SERVICE = "getDomainBasedEdgeSourceLabel"; //$NON-NLS-1$

    public static final String DOMAIN_BASED_EDGE_TARGET_LABEL_SERVICE = "getDomainBasedEdgeTargetLabel"; //$NON-NLS-1$

    public static final String RECONNECT_COMMENT_ANNOTATED_ELEMENT_EDGE_TARGET_SERVICE = "reconnectCommentAnnotatedElementEdgeTarget"; //$NON-NLS-1$

    public static final String RECONNECT_COMMENT_ANNOTATED_ELEMENT_EDGE_SOURCE_SERVICE = "reconnectCommentAnnotatedElementEdgeSource"; //$NON-NLS-1$

    public static final String RENDER_LABEL_SERVICE = "renderLabel";

    public static final String MOVE_IN = "moveIn"; //$NON-NLS-1$

    public static final String GET_SOURCE_SERVICE = "getSource"; //$NON-NLS-1$

    public static final String GET_TARGETS_SERVICE = "getTargets"; //$NON-NLS-1$

    public static final String GET_ALL_REACHABLE_SERVICE = "getAllReachable"; //$NON-NLS-1$

    public static final String DESTROY_SERVICE = "destroy"; //$NON-NLS-1$

    public static final String DROP_SERVICE = "drop"; //$NON-NLS-1$

    public static final String CREATE_SERVICE = "create"; //$NON-NLS-1$

    public static final String IS_NOT_VISUAL_DESCENDANT = "isNotVisualDescendant"; //$NON-NLS-1$

    public static final String REMOVE_VALUE_FROM = "removeValueFrom"; //$NON-NLS-1$

}

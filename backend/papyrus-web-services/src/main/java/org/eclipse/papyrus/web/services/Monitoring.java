/*******************************************************************************
 * Copyright (c) 2022 CEA.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.web.services;

/**
 * Monitoring constants.
 *
 * @author lfasani
 */
public final class Monitoring {

    public static final String EVENT_HANDLER = "papyrusweb_eventhandlers"; //$NON-NLS-1$

    public static final String NAME = "name"; //$NON-NLS-1$

    private Monitoring() {
        // Prevent instantiation
    }
}

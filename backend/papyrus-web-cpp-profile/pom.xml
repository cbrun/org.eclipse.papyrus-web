<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright (c) 2022 CEA, Obeo. This program and the accompanying materials 
	are made available under the terms of the Eclipse Public License v2.0 which 
	accompanies this distribution, and is available at https://www.eclipse.org/legal/epl-2.0/ 
	SPDX-License-Identifier: EPL-2.0 Contributors: Obeo - initial API and implementation -->
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>3.0.6</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>
	<groupId>papyrus-web</groupId>
	<artifactId>papyrus-web-cpp-profile</artifactId>
	<version>2023.3.0</version>
	<name>papyrus-web-cpp-profile</name>
	<description>Papyrus Web C++ profile</description>

	<properties>
		<java.version>17</java.version>
		<sirius.components.version>2023.6.4</sirius.components.version>
		<uml2.version>5.6.0-SNAPSHOT</uml2.version>
	</properties>

	<repositories>
		<repository>
			<id>Central</id>
			<url>https://repo.maven.apache.org/maven2</url>
		</repository>
		<repository>
			<id>github-sirius-components</id>
			<url>https://maven.pkg.github.com/eclipse-sirius/sirius-components</url>
		</repository>
		<repository>
			<id>github-papyrus-sirius-uml2</id>
			<url>https://maven.pkg.github.com/PapyrusSirius/uml2</url>
		</repository>
		<repository>
			<id>Papyrus Designer</id>
			<url>https://repo.eclipse.org/content/repositories/papyrus-snapshots/</url>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
			</snapshots>
		</repository>
	</repositories>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.uml</artifactId>
			<version>${uml2.version}</version>
		</dependency>
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-services</artifactId>
			<version>2023.3.0</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.uml.resources</artifactId>
			<version>5.5.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>papyrus-web</groupId>
			<artifactId>papyrus-web-representation-builder</artifactId>
			<version>2023.3.0</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.common</artifactId>
			<version>2.5.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.uml2.plugins</groupId>
			<artifactId>org.eclipse.uml2.types</artifactId>
			<version>2.5.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.papyrus.designer</groupId>
			<artifactId>org.eclipse.papyrus.designer.languages.cpp.profile</artifactId>
			<version>3.0.0-SNAPSHOT</version>
			<!-- Workaround for https://stackoverflow.com/questions/72625637/maven-build-failed-due-to-jdt-dependencies-no-versions-available-for-org-osgi -->
			<exclusions>
				<exclusion>
					<groupId>org.osgi</groupId> 
					<artifactId>org.osgi.service.prefs</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.eclipse.papyrus.designer</groupId>
			<artifactId>org.eclipse.papyrus.designer.languages.cpp.library</artifactId>
			<version>3.0.0-SNAPSHOT</version>
			<!-- Workaround for https://stackoverflow.com/questions/72625637/maven-build-failed-due-to-jdt-dependencies-no-versions-available-for-org-osgi -->
			<exclusions>
				<exclusion>
					<groupId>org.osgi</groupId> 
					<artifactId>org.osgi.service.prefs</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.8.7</version>
				<executions>
					<execution>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>report</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>

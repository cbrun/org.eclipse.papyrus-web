/*******************************************************************************
 * Copyright (c) 2021, 2023 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
import { NewObjectModal, NewRepresentationModal } from '@eclipse-sirius/sirius-components';
import { Selection } from '@eclipse-sirius/sirius-components-core';
import { TreeItemContextMenuComponentProps } from '@eclipse-sirius/sirius-components-trees';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import AddIcon from '@material-ui/icons/Add';
import { forwardRef, Fragment, useState } from 'react';
import { PublishProfileDialog } from './PublishProfileDialog';

type Modal = 'CreateNewObject' | 'CreateNewRepresentation' | 'PublishProfile';

export const ObjectTreeItemContextMenuContribution = forwardRef(
  (
    { editingContextId, item, readOnly, setSelection, expandItem, onClose }: TreeItemContextMenuComponentProps,
    ref: React.ForwardedRef<HTMLLIElement>
  ) => {
    const [modal, setModal] = useState<Modal>(null);

    const onObjectCreated = (selection: Selection) => {
      setSelection(selection);
      expandItem();
      onClose();
    };
    let modalElement = null;
    if (modal === 'CreateNewObject') {
      modalElement = (
        <NewObjectModal
          editingContextId={editingContextId}
          item={item}
          onObjectCreated={onObjectCreated}
          onClose={onClose}
        />
      );
    } else if (modal === 'CreateNewRepresentation') {
      modalElement = (
        <NewRepresentationModal
          editingContextId={editingContextId}
          item={item}
          onRepresentationCreated={onObjectCreated}
          onClose={onClose}
        />
      );
    } else if (modal === 'PublishProfile') {
      modalElement = <PublishProfileDialog editingContextId={editingContextId} item={item} onClose={onClose} />;
    }

    let applyProfileMenuItem = null;
    if (item.kind === 'siriusComponents://semantic?domain=uml&entity=Profile') {
      applyProfileMenuItem = (
        <MenuItem
          key="publish-profile"
          data-testid="publish-profile"
          onClick={() => setModal('PublishProfile')}
          disabled={readOnly}
          aria-disabled>
          <ListItemIcon>
            <AddIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Publish profile" />
        </MenuItem>
      );
    }

    return (
      <Fragment key="object-tree-item-context-menu-contribution">
        <MenuItem
          key="new-object"
          onClick={() => setModal('CreateNewObject')}
          data-testid="new-object"
          disabled={readOnly}
          ref={ref}
          aria-disabled>
          <ListItemIcon>
            <AddIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="New object" />
        </MenuItem>
        <MenuItem
          key="new-representation"
          onClick={() => setModal('CreateNewRepresentation')}
          data-testid="new-representation"
          disabled={readOnly}
          aria-disabled>
          <ListItemIcon>
            <AddIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="New representation" />
        </MenuItem>
        {applyProfileMenuItem}
        {modalElement}
      </Fragment>
    );
  }
);
